import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.filter('capitalize', function (value: string) {
  if (!value) return ''
  return value.charAt(0).toUpperCase() + value.slice(1)
})

new Vue({
  el: '#app',
  router,
  render: (h) => h(App),
})
